This is a simple Google Search project to test the usage of Java, Selenium, Cucumber, Junit,Maven.

To run this project:
1. Open the project in an IDE
2. Run the test runner file.

Directory Structure:

resources – contains the chrome driver used in the tests

results – Execution result from cucumber

config.properties – Contains the configurable test data variables like googlePageurl used in the project. These are read in step definition using the LoadConfiguration class methods

pom.xml – Maven’s pom file that has all the dependencies used in the framework

src – Source code

    test/features – contains the feature files (cucumber Gherkin language file)

    test/java – all the java implementations

    config/LoadConfiguration.java – Methods to read the configurations from config.properties

file


    pageFactory – PageFactory that contains all the pages used in Task1 and Task2

        BasePage.java – Super class of all Page classes and has the driver instance and all utility functions which are extensions of WebDriver

        GoogleSearchPage.java – All the elements of Google Search Page and methods that use these elements

        QAWikiPage.java – Elements and methods of Wiki page of QA

    stepDefinition – Contains the step definitions of all steps in the feature

        GoogleSearchSteps.java – All the step definitions of GoogleSearch feature specified in GoogleSearch.feature.

    base/BaseDriver.java - Contains the functions for web driver creation, quit. Used in stepDefinitions to create and use the webdriver. Future enhancement is to create webDriver with proper and specific capabilities.


TestRunner.java – Test runner file of cucumber. This specifies the tests to run.